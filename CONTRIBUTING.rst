=========================
How to contribute to MARV
=========================

Thank you for considering to contribute to MARV.

The MARV Community Edition is licensed under `AGPL-3.0
<https://www.gnu.org/licenses/agpl-3.0.html>`_ and we offer an
Enterprise Edition with an extended feature set under the proprietary
MARV-License.

For one we want to ensure that you have the rights to your
contributions, for another we'd like to be able to use contributions
to either edition in both editions.

To this end we require you to sign-off your commits to (1) certify the
origin of your contribution in the sense of the `Developer Certificate
of Origin <https://developercertificate.org/>`_ and to (2) indicate
that your contribution is licensed under the `Apache-2.0
<./LICENSES/Apache-2.0>`_ license for code and the `CC BY-SA 4.0
<https://creativecommons.org/licenses/by-sa/4.0/>`_ license for
documentation, beyond what is required by the license of the MARV
edition you are contributing to.


Signed-off-by lines
===================

By signing-off your commits you agree to the terms and conditions laid
out in this document, if applicable on behalf of your employer.

You sign-off a commit by adding the following line to the bottom of
its commit message, reflecting your real name and email address::

  Signed-off-by: Fullname <email@example.net>

Git does this automatically when using ``git commit -s``.

Except for the license granted herein to us and recipients of software
distributed by us, you reserve all right, title, and interest in and
to your contributions.


Developer Certificate of Origin
===============================

Embedded and reformatted from `Developer Certificate
of Origin <https://developercertificate.org/>`_:

Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
1 Letterman Drive
Suite D4700
San Francisco, CA, 94129

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

1. The contribution was created in whole or in part by me and I
   have the right to submit it under the open source license
   indicated in the file; or

2. The contribution is based upon previous work that, to the best
   of my knowledge, is covered under an appropriate open source
   license and I have the right under that license to submit that
   work with modifications, whether created in whole or in part
   by me, under the same open source license (unless I am
   permitted to submit under a different license), as indicated
   in the file; or

3. The contribution was provided directly to me by some other
   person who certified (1), (2) or (3) and I have not modified
   it.

4. I understand and agree that this project and the contribution
   are public and that a record of the contribution (including all
   personal information I submit with it, including my sign-off) is
   maintained indefinitely and may be redistributed consistent with
   this project or the open source license(s) involved.


License of contributions
========================

Beyond what is required by the license of the MARV edition a
contribution is made to, all contributions to this project are
licensed under the following licenses:

1. `Apache-2.0 <./LICENSES/Apache-2.0>`_ for code

2. `CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0/>`_ for documentation
